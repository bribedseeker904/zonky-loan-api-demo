# Zonky Loan API Demo

This application demonstrates using Zonky API to periodically fetch latest published loans.

The main component is the `RestLoanFetcher` class implements most of the core logic. The component
uses a `RestTemplateBuilder` to construct a customized `RestTemplate` for each API call.

Supporting components and classes, such as customizers and interceptors, are used by the main component
to enhance the API request calls.

The application context registered `UserAgentCustomizer` adds a correct `User-Agent` header to each request call based
on configured values (see below).

The generic unregistered `HeaderCustomizer` is used by `RestLoanFetcher` to add filtering to each request call.

## Zonky API

* [Marketplace](https://api.zonky.cz/loans/marketplace) lists available loans and allows paging, sorting and filtering
* [Apiary](https://zonky.docs.apiary.io) documents the API

## General Notes

The application was created using [Spring Initializr](https://start.spring.io) with the following dependencies only:

* Spring Boot (version 2.1.6)
* Spring Actuator to provide at least `/info` and `/health` endpoints
* Spring Web Starter to enable built-in Tomcat engine
* Quartz Scheduler to setup periodic job to fetch latest loans
* JUnit (version 4.12) for testing

## Building

To build the application you only need Java and Maven.

```
> mvn clean install
```

## Configuration

The application can be customized and configured by editing `application.properties` or adding additional
profiles.

### Setting up Zonky API

To successfully access Zonky API, the following property must be configured.

```properties
zonky.api.loans.marketplace=https://api.zonky.cz/loans/marketplace
```

### Setting up the periodic job

The following properties allows you to customize the period (in milliseconds) at which the job is scheduled and also
the period in minutes to consider published loans as new.

```properties
zonky.jobs.latest-loans.schedule-rate-millis=300000
zonky.jobs.latest-loans.published-for-at-least-minutes=5
```

## Things to do ... someday

For the purpose of simplicity and time concerns, some things are not implemented.

- [ ] Add support for paging loans. Currently only first 20 (by default) loans are fetched
- [ ] Add more properties to loans. Currently only a very basic subset of properties are modelled.
- [ ] Add unit tests for customizers and interceptors
- [ ] Add unit tests for loan fetcher from static data and/or mocked rest template
- [ ] Add health indicator to monitor the periodic job
- [ ] Add documentation to classes and interfaces
