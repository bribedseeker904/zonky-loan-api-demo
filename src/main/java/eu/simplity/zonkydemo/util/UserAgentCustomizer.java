package eu.simplity.zonkydemo.util;

import org.springframework.boot.web.client.RestTemplateCustomizer;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class UserAgentCustomizer implements RestTemplateCustomizer {

    private final UserAgentInterceptor userAgentInterceptor;

    public UserAgentCustomizer(UserAgentInterceptor userAgentInterceptor) {
        this.userAgentInterceptor = userAgentInterceptor;
    }

    @Override
    public void customize(RestTemplate restTemplate) {
        restTemplate.getInterceptors().add(userAgentInterceptor);
    }
}
