package eu.simplity.zonkydemo.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;

public class HeaderInterceptor implements ClientHttpRequestInterceptor {

    private static final Logger log = LoggerFactory.getLogger(HeaderInterceptor.class);

    private final String headerName;
    private final String headerValue;

    public HeaderInterceptor(String headerName, String headerValue) {
        this.headerName = headerName;
        this.headerValue = headerValue;
    }

    @Override
    public ClientHttpResponse intercept(HttpRequest httpRequest, byte[] bytes, ClientHttpRequestExecution execution) throws IOException {
        httpRequest.getHeaders().add(headerName, headerValue);
        if (log.isDebugEnabled()) {
            log.debug("Headers: {}", httpRequest.getHeaders());
            log.debug("URL: {}", httpRequest.getURI());
        }
        return execution.execute(httpRequest, bytes);
    }
}
