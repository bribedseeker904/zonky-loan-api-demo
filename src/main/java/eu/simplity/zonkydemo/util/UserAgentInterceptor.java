package eu.simplity.zonkydemo.util;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

import java.io.IOException;

import static java.lang.String.format;

@Component
public class UserAgentInterceptor implements ClientHttpRequestInterceptor {

    private static final String USER_AGENT_HEADER = "User-Agent";
    private static final String USER_AGENT_FORMAT = "%s/%s (%s)";

    private final UserAgent ua;

    public UserAgentInterceptor(UserAgent userAgent) {
        this.ua = userAgent;
    }

    @Override
    public ClientHttpResponse intercept(HttpRequest httpRequest, byte[] bytes, ClientHttpRequestExecution execution) throws IOException {
        httpRequest.getHeaders().add(USER_AGENT_HEADER, format(USER_AGENT_FORMAT, ua.getApplication(), ua.getVersion(), ua.getUrl()));
        return execution.execute(httpRequest, bytes);
    }
}
