package eu.simplity.zonkydemo.util;

import org.springframework.boot.web.client.RestTemplateCustomizer;
import org.springframework.web.client.RestTemplate;

/**
 * The {@code HeaderCustomizer} adds a specific value to HTTP request header. Internally, the customized rest template
 * is enhanced with {@link HeaderInterceptor} which actually adds the header.
 */
public class HeaderCustomizer implements RestTemplateCustomizer {

    private final String headerName;
    private final String headerValue;

    public HeaderCustomizer(String headerName, String headerValue) {
        this.headerName = headerName;
        this.headerValue = headerValue;
    }

    @Override
    public void customize(RestTemplate restTemplate) {
        restTemplate.getInterceptors().add(new HeaderInterceptor(headerName, headerValue));
    }
}
