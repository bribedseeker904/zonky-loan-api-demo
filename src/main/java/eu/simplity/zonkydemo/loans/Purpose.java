package eu.simplity.zonkydemo.loans;

public enum Purpose {
    OTHER,
    AUTO_MOTO,
    TRAVEL,
    HOUSEHOLD,
    HEALTH,
    REFINANCING,
    ELECTRONICS,
    OWN_PROJECT
}
