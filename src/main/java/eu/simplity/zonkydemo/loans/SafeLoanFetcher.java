package eu.simplity.zonkydemo.loans;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static java.util.Collections.emptyList;

/**
 * The {@code SafeLoanFetcher} is a convenient delegating {@link LoanFetcher} which catches all exceptions thrown
 * by the {@code delegate} and returns an empty list instead. All exceptions are just logged as errors.
 */
public class SafeLoanFetcher implements LoanFetcher {

    private static final Logger log = LoggerFactory.getLogger(SafeLoanFetcher.class);

    private final LoanFetcher delegate;

    public SafeLoanFetcher(LoanFetcher delegate) {
        this.delegate = delegate;
    }

    @Override
    public List<Loan> fetch(List<LoanFilter> filters, List<LoanSort> sorting) {
        try {
            return delegate.fetch(filters, sorting);
        } catch (Exception e) {
            log.error("Fetching loans failed.", e);
            return emptyList();
        }
    }
}
