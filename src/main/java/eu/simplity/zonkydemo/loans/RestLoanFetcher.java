package eu.simplity.zonkydemo.loans;

import eu.simplity.zonkydemo.util.HeaderCustomizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.joining;

/**
 * The {@code RestLoanFetcher} uses a {@link RestTemplate} to fetch {@link Loan}s from Zonky {@link LoanApi}.
 */
@Component
public class RestLoanFetcher implements LoanFetcher {

    private static final Logger log = LoggerFactory.getLogger(RestLoanFetcher.class);

    private final RestTemplateBuilder restTemplateBuilder;
    private final LoanApi loanApi;

    /**
     * Constructs this loan fetcher with a {@code restTemplateBuilder} and configured {@code loanApi}.
     *
     * @param restTemplateBuilder the {@link RestTemplateBuilder} to be used for creating {@link RestTemplate}s
     * @param loanApi             the {@link LoanApi} to build requests
     */
    public RestLoanFetcher(RestTemplateBuilder restTemplateBuilder, LoanApi loanApi) {
        this.restTemplateBuilder = restTemplateBuilder;
        this.loanApi = loanApi;
    }

    @Override
    public List<Loan> fetch(List<LoanFilter> filters, List<LoanSort> sorting) {
        RestTemplate restTemplate = restTemplateBuilder
                .additionalCustomizers(new HeaderCustomizer("X-Order", sorting.stream().map(LoanSort::toHeaderString).collect(joining(","))))
                .build();
        String query = "?" + filters.stream().map(LoanFilter::toQueryString).collect(joining("&"));
        try {
            Loan[] loans = restTemplate.getForObject(loanApi.getMarketplace() + query, Loan[].class);
            return Arrays.asList(loans);
        } catch (RestClientResponseException e) {
            log.error(e.getMessage(), e);
            throw new FetchFailedException(filters, sorting, HttpStatus.resolve(e.getRawStatusCode()));
        } catch (RestClientException e) {
            log.error(e.getMessage(), e);
            throw new FetchFailedException(filters, sorting, HttpStatus.BAD_REQUEST); // we made a wrong call
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new FetchFailedException(filters, sorting, HttpStatus.INTERNAL_SERVER_ERROR); // something else went wrong
        }
    }
}
