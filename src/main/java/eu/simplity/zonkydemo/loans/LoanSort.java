package eu.simplity.zonkydemo.loans;

public class LoanSort {

    private final String field;
    private final Direction direction;

    public LoanSort(String field, Direction direction) {
        this.field = field;
        this.direction = direction;
    }

    public String toHeaderString() {
        return direction == Direction.ASCENDING ? field : "-" + field;
    }

    public static LoanSort asc(String field) {
        return new LoanSort(field, Direction.ASCENDING);
    }

    public static LoanSort desc(String field) {
        return new LoanSort(field, Direction.DESCENDING);
    }

    public enum Direction {
        ASCENDING,
        DESCENDING
    }
}
