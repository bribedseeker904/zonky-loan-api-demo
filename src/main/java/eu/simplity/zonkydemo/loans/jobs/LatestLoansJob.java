package eu.simplity.zonkydemo.loans.jobs;

import eu.simplity.zonkydemo.loans.Loan;
import eu.simplity.zonkydemo.loans.LoanFetcher;
import eu.simplity.zonkydemo.loans.LoanFields;
import eu.simplity.zonkydemo.loans.LoanFilter;
import eu.simplity.zonkydemo.loans.LoanSort;
import eu.simplity.zonkydemo.loans.SafeLoanFetcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static java.util.Arrays.asList;

@Component
public class LatestLoansJob {

    private static final Logger log = LoggerFactory.getLogger(LatestLoansJob.class);

    private final LoanFetcher fetcher;
    private final LatestLoansJobProperties properties;

    public LatestLoansJob(LoanFetcher fetcher, LatestLoansJobProperties properties) {
        this.fetcher = fetcher;
        this.properties = properties;
    }

    @Scheduled(fixedRateString = "${zonky.jobs.latest-loans.schedule-rate-millis}")
    public void printLatest() {
        Instant since = Instant.now().minus(properties.getPublishedForAtLeastMinutes(), ChronoUnit.MINUTES);
        LoanFilter publishedSince = new LoanFilter(LoanFields.DATE_PUBLISHED, since.toString(), LoanFilter.Operation.GREATER_OR_EQUAL);
        LoanSort byPublishDate = LoanSort.desc(LoanFields.DATE_PUBLISHED);
        List<Loan> loans = new SafeLoanFetcher(fetcher).fetch(asList(publishedSince), asList(byPublishDate));
        log.info("Found {} loans since {}", loans.size(), since);
        loans.forEach(loan -> log.info("New loan {} published at {}", loan.getId(), loan.getDatePublished()));
    }
}
