package eu.simplity.zonkydemo.loans.jobs;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("zonky.jobs.latest-loans")
public class LatestLoansJobProperties {

    private long scheduleRateMillis = 300000L; // five minutes rate interval
    private long publishedForAtLeastMinutes = 5; // fetch loans published for at least X minutes

    public long getScheduleRateMillis() {
        return scheduleRateMillis;
    }

    public void setScheduleRateMillis(long scheduleRateMillis) {
        this.scheduleRateMillis = scheduleRateMillis;
    }

    public long getPublishedForAtLeastMinutes() {
        return publishedForAtLeastMinutes;
    }

    public void setPublishedForAtLeastMinutes(long publishedForAtLeastMinutes) {
        this.publishedForAtLeastMinutes = publishedForAtLeastMinutes;
    }
}
