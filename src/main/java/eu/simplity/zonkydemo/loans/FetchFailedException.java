package eu.simplity.zonkydemo.loans;

import org.springframework.http.HttpStatus;

import java.util.List;

public class FetchFailedException extends RuntimeException {

    private final List<LoanFilter> filters;
    private final List<LoanSort> sorting;
    private final HttpStatus httpStatus;

    public FetchFailedException(List<LoanFilter> filters, List<LoanSort> sorting, HttpStatus httpStatus) {
        this.filters = filters;
        this.sorting = sorting;
        this.httpStatus = httpStatus;
    }
}
