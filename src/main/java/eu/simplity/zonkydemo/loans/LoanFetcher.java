package eu.simplity.zonkydemo.loans;

import java.util.List;

/**
 * The {@code LoanFetcher} provides simple to use methods to fetch {@link Loan}s.
 */
public interface LoanFetcher {

    /**
     * Fetch {@link Loan}s using given {@code filters} and {@code sorting}
     *
     * @param filters list of filters to use
     * @param sorting list of sorting to use
     * @return a list of loans
     */
    List<Loan> fetch(List<LoanFilter> filters, List<LoanSort> sorting);

}
