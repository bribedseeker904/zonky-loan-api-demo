package eu.simplity.zonkydemo.loans;

abstract public class LoanFields {

    public static final String ID = "id";
    public static final String DATE_PUBLISHED = "datePublished";

    // ... more fields should be here
}
