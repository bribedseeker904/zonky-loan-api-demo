package eu.simplity.zonkydemo.loans;

import static java.lang.String.format;

public class LoanFilter {
    private final String field;
    private final String value;
    private final Operation operation;

    public LoanFilter(String field, String value, Operation operation) {
        this.field = field;
        this.value = value;
        this.operation = operation;
    }

    public String getField() {
        return field;
    }

    public String getValue() {
        return value;
    }

    public Operation getOperation() {
        return operation;
    }

    public String toQueryString() {
        return format("%s__%s=%s", field, operation.suffix, value);
    }

    public enum Operation {
        GREATER_THAN("gt"),
        GREATER_OR_EQUAL("gte");

        private final String suffix;

        Operation(String suffix) {
            this.suffix = suffix;
        }

        public String getSuffix() {
            return suffix;
        }
    }
}
