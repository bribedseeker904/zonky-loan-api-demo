package eu.simplity.zonkydemo.loans;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.web.client.RestTemplateBuilder;

import java.util.List;

import static java.util.Arrays.asList;

public class RestLoanFetcherTest { // this works more as an integration test

    private RestLoanFetcher fetcher;

    @Before
    public void setup() {
        LoanApi api = new LoanApi();
        api.setMarketplace("https://api.zonky.cz/loans/marketplace");

        fetcher = new RestLoanFetcher(new RestTemplateBuilder(), api);
    }

    @Test
    public void getSortedPublishedAsOfToday() {
        List<Loan> loans = fetcher.fetch(
                asList(new LoanFilter("datePublished", "2019-07-01", LoanFilter.Operation.GREATER_OR_EQUAL)),
                asList(LoanSort.desc("datePublished")));
        Assert.assertFalse(loans.isEmpty()); // only checking results are empty
    }

}
