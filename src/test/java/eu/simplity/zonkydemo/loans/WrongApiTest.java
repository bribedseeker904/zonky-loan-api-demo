package eu.simplity.zonkydemo.loans;

import org.junit.Test;
import org.springframework.boot.web.client.RestTemplateBuilder;

import static java.util.Arrays.asList;

public class WrongApiTest {

    @Test(expected = FetchFailedException.class)
    public void failOnWrongAPI() {
        LoanApi api = new LoanApi();
        api.setMarketplace("https://api.zonky.cz/loans/xxxxx");

        new RestLoanFetcher(new RestTemplateBuilder(), api).fetch(
                asList(new LoanFilter("datePublished", "2019-07-01", LoanFilter.Operation.GREATER_OR_EQUAL)),
                asList(LoanSort.desc("datePublished")));
        // the above should fail
    }
}
