package eu.simplity.zonkydemo;

import eu.simplity.zonkydemo.loans.LoanApi;
import eu.simplity.zonkydemo.util.UserAgent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ZonkyDemoApplicationTests {

    @Autowired
    private UserAgent userAgent;

    @Autowired
    private LoanApi loanApi;

    @Test
    public void contextLoads() {
    }

    @Test
    public void userAgentIsConfigured() {
        assertEquals("SimplityDemo", userAgent.getApplication());
        assertEquals("1.0", userAgent.getVersion());
        assertEquals("https://gitlab.com/bribedseeker904/zonky-loan-api-demo", userAgent.getUrl());
    }

    @Test
    public void loanApiIsConfigured() {
        assertEquals("https://api.zonky.cz/loans/marketplace", loanApi.getMarketplace());
    }
}
